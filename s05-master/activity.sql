-- MySQL S5 Activity:

-- 1. Return the customerName of the customers who are from the Philippines
SELECT customers.customerName from customers WHERE country = "Philippines";


-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT customers.contactLastName, customers.contactFirstName from customers WHERE customerName = "La Rochelle Gifts";



-- 3. Return the product name and MSRP of the product named "The Titanic"
SELECT products.productName, products.MSRP from products WHERE productName = "The Titanic";



-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT employees.firstName, employees.lastName from employees WHERE email = "jfirrelli@classicmodelcars.com";


-- 5. Return the names of customers who have no registered state
SELECT customers.customerName from customers WHERE state IS NULL;


-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
SELECT employees.firstName, employees.lastName, employees.email from employees where lastName = "Patterson" AND firstName = "Steve";



-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT customers.customerName, customers.country, customers.creditLimit from customers where country <> "USA" AND creditLimit > 3000;



-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'
SELECT orders.customerNumber from orders where comments LIKE "%DHL%";



-- 9. Return the product lines whose text description mentions the phrase 'state of the art'
SELECT productlines.productLine from productlines where textDescription LIKE "%state of the art%";



-- 10. Return the countries of customers without duplication
SELECT DISTINCT customers.country from customers;


-- 11. Return the statuses of orders without duplication
SELECT DISTINCT orders.status from orders;


-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT DISTINCT customers.customerName, customers.country from customers where country = "USA" OR country = "FRANCE" OR country = "CANADA";



-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo
SELECT employees.firstName, employees.lastName, offices.city from employees
    JOIN offices ON employees.officeCode = offices.officeCode
    WHERE offices.city = "Tokyo";


-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"
SELECT customers.customerName from customers
    JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
    WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";


-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"
SELECT products.productName, customers.customerName from orders
    JOIN customers ON orders.customerNumber = customers.customerNumber
    JOIN orderdetails ON  orders.orderNumber = orderdetails.orderNumber
    JOIN products ON  orderdetails.productCode = products.productCode
    WHERE customers.customerName = 'Baane Mini Imports';



-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
SELECT DISTINCT employees.firstName, employees.lastName, customers.customerName, offices.country from payments
    JOIN customers ON  payments.customerNumber = customers.customerNumber
    JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
    JOIN offices ON employees.officeCode = offices.officeCode
    WHERE customers.country = offices.country;



-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000

SELECT products.productName, products.quantityInStock from products
    JOIN productlines ON products.productLine = productlines.productLine
    WHERE productlines.productLine = "planes" AND products.quantityInStock < 1000;


-- 18. Show the customer's name with a phone number containing "+81".
SELECT customers.customerName from customers WHERE customers.phone LIKE "+81%";
